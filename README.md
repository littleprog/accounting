# accounting

#### 介绍
财会学习

基于node.js实现《世界上最简单的会计书》中的内容

本猿对数字非常头大, 看了这<最简单>系列一会儿之后仍然是昏昏欲睡哈欠不止. 尤其是前面提过的数字后面再扣减, 还得向前翻页查找. 开始使用Excel来做表, 然而发现在讲解资产负债表的过程中需要按经营实际对原表不断修改, 中间过程在表格中就被抹杀看不到了. 另一个是要时时保持资产负债表左右平衡, 本猿我习惯了让程序计算, 实在是不愿做这种重复劳动, 哈哈.

按惯例, 你应该安装node.js. 装完以后, 在目录下运行 `node .`就可以了. 运行结果主要反映主要经营事件和每周三大表的输出.
主要源码在`index.js`中, 主要是以**中文**编写的(没错! 就是中文!不需要什么中文编程语言, JS大法就能支持中文), 需要了解每种事件发生时是如何影响三大表(主要是资产负债表)的话, 请阅读源码哦. 
`color.js`是为了输出增加点彩色, 可以无视.

最后一章给人带来的思考真的很不错, 我的总结如下: 
    * 三大比率: 成本收入比, 费用收入比, 净利率, 应该与行业均值比较, 并且与自身过往比较(趋势分析);
    * 固定资产的投资最终可能很难收回, 因为固定资产(尤其是设备)都不大好卖的, 因此固定资产应该慎重投资, 应评估固定资产投入后能带来多少产出, 利润是否能很快覆盖并超出固定资产;
    * 对于供应链管理, 先进先出, 减少存货库存周期及其重要. 库存长期积压, 无论对于保质期长短的存货都不是好事, 长期积压最后会直接导致资产减值;
    * 应减少坏账, 评估客户和渠道的偿债能力, 坏账将直接导致利润减值;
    * 现金流及其重要, 更甚于利润. 一个盈利良好的企业现金流一旦中断, 企业连一天都不能维持.
    * 利润存在于账面上, 是很容易流失的;
    * 提高利润其实就是老生常谈的开源节流,降本增效. 但开源和节流是矛盾的, 降低成本和费用也有可能导致销量下降(广告曝光不足/品质下降等), 而开发新产品新功能一定是会提高成本的.

最后, 这本书原书真是不错, 适合小白入门. 不过翻译错误也不少, 幸好有落叶大大的[勘误与问题](https://book.douban.com/review/8520722/)参照, 为计算提供很大帮助. 翻译之瑕不掩原书之瑜, 还是给个五分!