color = {
    
        还原: "\x1b[0m",
        暗: "\x1b[2m",
        黑: "\x1b[30m",
        红: "\x1b[31m",
        绿: "\x1b[32m",
        黄: "\x1b[33m",
        蓝: "\x1b[34m",
        紫: "\x1b[35m",
        青: "\x1b[36m",
        白: "\x1b[37m",

    背景: {
        黑: "\x1b[40m",
        红: "\x1b[41m",
        绿: "\x1b[42m",
        黄: "\x1b[43m",
        蓝: "\x1b[44m",
        紫: "\x1b[45m",
        青: "\x1b[46m",
        白: "\x1b[47m"
    },

}

color.打印标题 = function(s){
    console.log('\n' + color.背景.红 + s + color.还原)
}
color.打印事件 = function(s){
    console.log('\n' + color.绿 + s + color.还原)
}
module.exports = color